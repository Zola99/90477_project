In this folder you can find data gathered from various sources. Available data sets are:

- Milan contains data for daily average tempeature, relative humidity, PM levels and Air Quality Index;
- Wind_speed_2020 contains data for average wind speed for 2020 according to ilmeteo.net;
- Covid_cases contains data for total cumulative cases according to Protezione Civile.
